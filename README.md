
## Self-made BSgenome Package for UCSC gorGor5 Genome Assembly

The source data is downloaded from [here](ftp://hgdownload.cse.ucsc.edu/goldenPath/currentGenomes/Gorilla_gorilla_gorilla/bigZips/gorGor5.2bit).

The seed file is located at `inst/extdata/BSgenome.GGorilla.UCSC.gorGor5-seed`

To install, use

```r
devtools::install_git('https://gitlab.com/Marlin-Na/BSgenome.GGorilla.UCSC.gorGor5.git')
```

