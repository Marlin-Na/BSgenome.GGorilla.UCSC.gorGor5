\name{BSgenome.GGorilla.UCSC.gorGor5}
\docType{package}

\alias{BSgenome.GGorilla.UCSC.gorGor5-package}
\alias{BSgenome.GGorilla.UCSC.gorGor5}
\alias{GGorilla}

\title{Full genome sequences for Gorilla gorilla (UCSC version gorGor5)}

\description{
  Full genome sequences for Gorilla gorilla as provided by UCSC (gorGor5) and stored in Biostrings objects.
}

\details{
  
}

\note{
  This BSgenome data package was made from the following source data files:
  \preformatted{
ftp://hgdownload.soe.ucsc.edu/goldenPath/gorGor5/bigZips/gorGor5.2bit
  }

  See \code{?\link[BSgenome]{BSgenomeForge}} and the BSgenomeForge
  vignette (\code{vignette("BSgenomeForge")}) in the \pkg{BSgenome}
  software package for how to make a BSgenome data package.
}

\author{The Bioconductor Dev Team}

\seealso{
  \itemize{
    \item \link[BSgenome]{BSgenome} objects and the
          \code{\link[BSgenome]{available.genomes}} function
          in the \pkg{BSgenome} software package.
    \item \link[Biostrings]{DNAString} objects in the \pkg{Biostrings}
          package.
    \item The BSgenomeForge vignette (\code{vignette("BSgenomeForge")})
          in the \pkg{BSgenome} software package for how to make a BSgenome
          data package.
  }
}

\examples{
BSgenome.GGorilla.UCSC.gorGor5
genome <- BSgenome.GGorilla.UCSC.gorGor5
head(seqlengths(genome))


## ---------------------------------------------------------------------
## Genome-wide motif searching
## ---------------------------------------------------------------------
## See the GenomeSearching vignette in the BSgenome software
## package for some examples of genome-wide motif searching using
## Biostrings and the BSgenome data packages:
if (interactive())
    vignette("GenomeSearching", package="BSgenome")
}

\keyword{package}
\keyword{data}
